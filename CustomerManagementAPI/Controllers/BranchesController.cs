﻿using CustomerManagementAPI.Data;
using CustomerManagementAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CustomerManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private readonly ApplicationDbContext _db;

        public BranchesController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<Branch>> GetAll()
        {
            var branches = _db.Branches.ToList();
            return Ok(branches);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Branch> Get(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var branch = _db.Branches.FirstOrDefault(b => b.BranchId == id);
            if (branch == null)
            {
                return NotFound();
            }
            return Ok(branch);
        }
        
        [HttpPost("create")]
        public ActionResult<Branch> Add(Branch branch)
        {
            _db.Branches.Add(branch);
            _db.SaveChanges();
            return CreatedAtAction(nameof(Get), new { id = branch.BranchId }, branch);
        }

        [HttpDelete("delete/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var branch = _db.Branches.FirstOrDefault(b => b.BranchId == id);
            if (branch == null)
            {
                return NotFound();
            }
            _db.Branches.Remove(branch);
            _db.SaveChanges();
            return NoContent();
        }

        [HttpPut("edit/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult Edit(int id, Branch newBranch)
        {
            if (newBranch == null || id != newBranch.BranchId)
            {
                return BadRequest();
            }
            _db.Branches.Update(newBranch);
            _db.SaveChanges();
            return Ok(newBranch);
            
        }
    }

}
